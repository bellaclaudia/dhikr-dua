import 'package:flutter/material.dart';
import 'package:flutter_inamart/page/feedback.dart';
import 'package:flutter_inamart/page/home.dart';
import 'package:flutter_inamart/page/tasbihcounter.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), bottomRight: Radius.circular(50)),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[buildHeader(context), buildMenuItems(context)],
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Container(
        color: Colors.blue.shade700,
        padding: EdgeInsets.only(
          top: 24 + MediaQuery.of(context).padding.top,
          bottom: 24,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10),
              height: 70,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage('assets/logo.png'),
                ),
              ),
            ),
            const SizedBox(height: 12),
            const Text(
              "Life With Allah",
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255), fontSize: 25),
            ),
            const Text(
              "versi 1.0",
              style: TextStyle(
                  color: Color.fromARGB(255, 47, 47, 48), fontSize: 15),
            ),
          ],
        ),
      );

  Widget buildMenuItems(BuildContext context) => Container(
        padding: const EdgeInsets.all(20),
        child: Wrap(
          children: [
            ListTile(
              leading: const Icon(Icons.dashboard),
              title: const Text('Home'),
              onTap: () =>
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => const HomePage(),
              )),
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.arrow_circle_right),
              title: const Text('Tasbih Counter'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const TasbihCounterPage(),
                ));
              },
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.note_alt_outlined),
              title: const Text('Our Publications'),
              onTap: () {},
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.share),
              title: const Text('Share Our App'),
              onTap: () {},
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.star_border),
              title: const Text('Rate Us'),
              onTap: () {},
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.chat_bubble_outline_outlined),
              title: const Text('Feedback'),
              onTap: () =>
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => const FeedbackPage(),
              )),
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.info_outline),
              title: const Text('About Life With Allah'),
              onTap: () {},
            ),
            const Divider(color: Colors.grey),
          ],
        ),
      );
}
