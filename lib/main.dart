// import 'dart:io';
import 'dart:io';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/foundation.dart' show FlutterError, kReleaseMode;
import 'package:flutter/material.dart';
import 'package:flutter_inamart/page/home.dart';
import 'package:flutter_inamart/page/articles.dart';
import 'package:flutter_inamart/page/emotions.dart';
import 'package:flutter_inamart/page/favorites.dart';
import 'package:flutter_inamart/page/reminder.dart';
import 'package:flutter_inamart/navigation_drawer.dart';
// ignore: depend_on_referenced_packages
// import 'package:firebase_core/firebase_core.dart';

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp(
  //   options: const FirebaseOptions(
  //     apiKey: "AIzaSyBXuAgsquftNRuUoXHAJ9qH85j1ky78MTA",
  //     appId: "1:750435080218:android:713b6fd755ddb3737259a3",
  //     messagingSenderId: "750435080218",
  //     projectId: "flutter-inamart-536e3",
  //   ),
  // );
  FlutterError.onError = (details) {
    FlutterError.presentError(details);
    if (kReleaseMode) exit(1);
  };
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) {
        Widget error = const Text('...rendering error...');
        if (widget is Scaffold || widget is Navigator) {
          error = Scaffold(body: Center(child: error));
        }
        ErrorWidget.builder = (errorDetails) => error;
        if (widget != null) return widget;
        throw ('widget is null');
      },
      title: 'Dhikr & Dua',
      // Set Raleway as the default app font.
      theme: ThemeData(
          fontFamily: 'BrandonText',
          appBarTheme: const AppBarTheme(
            color: Color.fromARGB(255, 10, 4, 66),
          )),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  int index = 0;

  final screens = [
    const HomePage(),
    const FavoritesPage(),
    const ArticlesPage(),
    const EmotionsPage(),
    const ReminderPage(),
  ];

  @override
  Widget build(BuildContext context) {
    // final items = <Widget>[
    //   const Icon(
    //     Icons.home,
    //     size: 30,
    //     color: Colors.white,
    //   ),
    //   const Icon(
    //     Icons.favorite_outline,
    //     size: 30,
    //     color: Colors.white,
    //   ),
    //   const Icon(
    //     Icons.note_alt,
    //     size: 30,
    //     color: Colors.white,
    //   ),
    //   const Icon(
    //     Icons.emoji_emotions,
    //     size: 30,
    //     color: Colors.white,
    //   ),
    //   const Icon(
    //     Icons.calendar_today,
    //     size: 30,
    //     color: Colors.white,
    //   ),
    // ];
    return Container(
      color: Colors.grey,
      child: SafeArea(
        top: false,
        child: ClipRect(
          child: Scaffold(
            drawer: const NavigationDrawer(),
            // body: Center(
            //   child: Column(
            //     children: [
            //       Text(
            //         '$index',
            //         style: TextStyle(
            //             color: Colors.black,
            //             fontSize: 120,
            //             fontWeight: FontWeight.bold),
            //       ),
            //       SizedBox(height: 16),
            //       ElevatedButton(
            //           style: ElevatedButton.styleFrom(
            //               onPrimary: Colors.black,
            //               primary: Colors.white,
            //               minimumSize: Size(250, 56)),
            //           onPressed: () {
            //             final navigationState = navigationKey.currentState!;

            //             navigationState.setPage(2);
            //           },
            //           child: Text(
            //             'Go to 0',
            //             style: TextStyle(fontSize: 20),
            //           ))
            //     ],
            //   ),
            // ),
            body: screens[index],
            // bottomNavigationBar: Theme(
            //   data: Theme.of(context).copyWith(
            //       iconTheme: IconThemeData(color: Colors.grey.shade300)),
            //   child: CurvedNavigationBar(
            //     index: index,
            //     key: navigationKey,
            //     backgroundColor: Colors.transparent,
            //     color: Colors.grey.shade300,
            //     buttonBackgroundColor: Colors.blue.shade700,
            //     height: 60,
            //     animationCurve: Curves.easeInOut,
            //     animationDuration: const Duration(milliseconds: 300),
            //     items: items,
            //     onTap: (index) => setState(() => this.index = index),
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
