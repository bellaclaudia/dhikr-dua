import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inamart/navigation_drawer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class FeedbackPage extends StatefulWidget {
  const FeedbackPage({Key? key}) : super(key: key);

  @override
  State<FeedbackPage> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  final formKey = GlobalKey<FormState>();
  String email = '';
  String feedback = '';

  bool isLoading = false;

  @override
  Widget build(BuildContext context) => isLoading
      ? const LoadingPage()
      : Scaffold(
          drawer: const NavigationDrawer(),
          extendBody: true,
          appBar: AppBar(
            backgroundColor: Colors.blue.shade700,
            centerTitle: true,
            elevation: 0,
            title: const Text('Feedback', textAlign: TextAlign.center),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.settings_applications_sharp),
                // tooltip: 'Show Snackbar',
                onPressed: () {},
              ),
            ],
          ),
          body: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: formKey,
            child: ListView(
              padding: const EdgeInsets.all(16),
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email_outlined),
                      labelText: 'Email Address',
                      border: OutlineInputBorder()),
                  validator: (email) =>
                      email != null && !EmailValidator.validate(email)
                          ? 'Enter a valid Email'
                          : null,
                ),
                const SizedBox(height: 32),
                TextFormField(
                  decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(40),
                      prefixIcon: Icon(Icons.notes),
                      labelText: 'Feedback',
                      border: OutlineInputBorder()),
                  validator: (value) {
                    if (value != null && value.length < 5) {
                      return 'Fedback is required';
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(height: 32),
                ElevatedButton(
                  onPressed: () async {
                    final isValidForm = formKey.currentState!.validate();
                    setState(() => isLoading = true);
                    await Future.delayed(const Duration(seconds: 3));
                    setState(() => isLoading = false);

                    if (isValidForm) {}
                  },
                  child: const Text('Send'),
                ),
              ],
            ),
          ),
        );

  // ignore: dead_code
  // Widget buildEmail() => TextField(
  //       decoration: const InputDecoration(
  //           labelText: 'Email', border: OutlineInputBorder()),
  //       keyboardType: TextInputType.emailAddress,
  //       onChanged: (value) => setState(() => email = value),
  //     );
}

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Center(
            child: SpinKitCircle(
              size: 100,
              itemBuilder: (context, index) {
                final colors = [Colors.pink, Colors.blue.shade300];
                final color = colors[index % colors.length];
                return DecoratedBox(
                  decoration: BoxDecoration(
                    color: color,
                    shape: BoxShape.circle,
                  ),
                );
              },
            ),
          ),
          // const Text(
          //     'Thank you! Your feedback is valuable for us. It will help us improve Dhikr & Dua app.',
          //     textAlign: TextAlign.center),
          // const SizedBox(height: 32),
          // ElevatedButton(
          //   onPressed: () {},
          //   child: const Text('OK'),
          // ),
        ],
      ),
    );
  }
}
