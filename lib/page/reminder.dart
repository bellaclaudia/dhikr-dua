import 'package:flutter/material.dart';
import 'package:flutter_inamart/navigation_drawer.dart';

class ReminderPage extends StatelessWidget {
  const ReminderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: const Text('Tasbih Counter'),
          backgroundColor: Colors.blue.shade700,
        ),
      );
}
