import 'package:flutter/material.dart';
import 'package:flutter_inamart/navigation_drawer.dart';

class ArticlesPage extends StatefulWidget {
  const ArticlesPage({super.key});

  @override
  State<ArticlesPage> createState() => _ArticlesPageState();
}

class _ArticlesPageState extends State<ArticlesPage> {
  List<Menu1> menus1 = [
    const Menu1(
        judul: 'Dhikr',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: "Du`a'",
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Morning & Evening',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: "Qur'an",
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Sweetness of Salah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Salawat',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Istighfar',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Eqiquettes',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Death',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Ruqyah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Names of Allah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Ramadan',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Actions of the Heart',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Seasons of Worship',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Muharram',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu1(
        judul: 'Dhul Hijjah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
  ];
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.blue.shade700,
          centerTitle: true,
          elevation: 0,
          title: const Text('Articles', textAlign: TextAlign.center),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.settings_applications_sharp),
              // tooltip: 'Show Snackbar',
              onPressed: () {},
            ),
          ],
        ),
        body: ListView.builder(
          itemCount: menus1.length,
          itemBuilder: (context, index) {
            final menu = menus1[index];
            return Card(
              child: ListTile(
                leading: CircleAvatar(
                  radius: 16,
                  backgroundImage: NetworkImage(menu.urlAvatar),
                ),
                title: Text(menu.judul),
                trailing: const Icon(Icons.arrow_forward),
              ),
            );
          },
        ),
      );
}

class Menu1 {
  final String judul;
  final String urlAvatar;

  const Menu1({
    required this.judul,
    required this.urlAvatar,
  });
}
