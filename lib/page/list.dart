import 'package:flutter/material.dart';
import 'package:flutter_inamart/page/home.dart';
import 'package:flutter_inamart/page/play.dart';

// ignore: must_be_immutable
class ListPage extends StatelessWidget {
  final Menu menu;
  ListPage({
    Key? key,
    required this.menu,
  }) : super(key: key);

  List<ListMenu> listmenus = [
    const ListMenu(judul: 'Ayat al-Kursi', subjudul: 'The Greatest Protection'),
    const ListMenu(judul: '3 Quls', subjudul: 'Sufficed in All Your Matters'),
    const ListMenu(
        judul: 'Sayyid al-Istighfar',
        subjudul: 'The Best Way of Seeking Forgiveness'),
  ];
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue.shade700,
          title: Text(menu.judul),
        ),
        body: ListView.builder(
          itemCount: listmenus.length,
          itemBuilder: (context, index) {
            final listmenu = listmenus[index];
            return Card(
              child: ListTile(
                title: Text(listmenu.judul),
                subtitle: Text(listmenu.subjudul),
                trailing: const Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => PlayPage(listmenu: listmenu),
                    ),
                  );
                },
              ),
            );
          },
        ),
      );
}

class ListMenu {
  final String judul;
  final String subjudul;

  const ListMenu({required this.judul, required this.subjudul});
}
