import 'package:flutter/material.dart';
import 'package:flutter_inamart/page/list.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class PlayPage extends StatefulWidget {
  final ListMenu listmenu;
  const PlayPage({
    Key? key,
    required this.listmenu,
  }) : super(key: key);

  @override
  State<PlayPage> createState() => _PlayPageState();
}

class _PlayPageState extends State<PlayPage> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  int index = 2;
  final audioPlayer = AudioPlayer();
  bool isPlaying = false;
  Duration duration = Duration.zero;
  Duration position = Duration.zero;

  @override
  void initState() {
    super.initState();

    audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        isPlaying = state == PlayerState.PLAYING;
      });
    });

    audioPlayer.onDurationChanged.listen((newDuration) {
      setState(() {
        duration = newDuration;
      });
    });

    audioPlayer.onAudioPositionChanged.listen((newPosition) {
      setState(() {
        position = newPosition;
      });
    });
  }

  @override
  void dispose() {
    audioPlayer.dispose();

    super.dispose();
  }

  String formatTime(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final hours = twoDigits(duration.inHours);
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));

    return [
      if (duration.inHours > 0) hours,
      minutes,
      seconds,
    ].join(':');
  }

  @override
  Widget build(BuildContext context) {
    final items = <Widget>[
      const Icon(
        Icons.play_arrow,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.info,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.one_k,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.settings,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.favorite,
        size: 30,
        color: Colors.white,
      ),
    ];
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue.shade700,
          title: const Text('Al-kursi'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // ClipRRect(
              //   borderRadius: BorderRadius.circular(20),
              //   child: Image.network(
              //     'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg',
              //     width: double.infinity,
              //     height: 350,
              //     fit: BoxFit.cover,
              //   ),
              // ),
              // const SizedBox(height: 32),
              // const Padding(
              //   padding: EdgeInsets.all(5),
              // ),
              const Text(
                  "للَّهُ لَآ إِلَٰهَ إِلَّا هُوَ ٱلْحَىُّ ٱلْقَيُّومُ ۚ لَا تَأْخُذُهُۥ سِنَةٌ وَلَا نَوْمٌ ۚ لَّهُۥ مَا فِى ٱلسَّمَٰوَٰتِ وَمَا فِى ٱلْأَرْضِ ۗ مَن ذَا ٱلَّذِى يَشْفَعُ عِندَهُۥٓ إِلَّا بِإِذْنِهِۦ ۚ يَعْلَمُ مَا بَيْنَ أَيْدِيهِمْ وَمَا خَلْفَهُمْ ۖ وَلَا يُحِيطُونَ بِشَىْءٍ مِّنْ عِلْمِهِۦٓ إِلَّا بِمَا شَآءَ ۚ وَسِعَ كُرْسِيُّهُ ٱلسَّمَٰوَٰتِ وَٱلْأَرْضَ ۖ وَلَا يَـُٔودُهُۥ حِفْظُهُمَا ۚ وَهُوَ ٱلْعَلِىُّ ٱلْعَظِيمُ"),
              const Padding(
                padding: EdgeInsets.all(5),
              ),
              const Text(
                  "Allohu laa ilaaha illaa Huwal Hayyul Qoyyuum, laa ta’khudzuhuu sinatuw walaa nauum, la Huu maa fis samawaati wa maa fil ardh, mann dzalladzii yasyfa’u ‘inda Huu, illa bi idznih, ya’lamu maa bayna aidiihim wa maa kholfahum, wa laa yuhiituuna bisyayim min ‘ilmi Hii illaa bi maa syaa’, wa si’a kursiyyuus samaawaati walardh, wa laa yauudlu Huu hifdzuhumaa, wa Huwal ‘aliyyul ‘adziiim"),
              const Padding(
                padding: EdgeInsets.all(5),
              ),
              const Text(
                  "Allah, tidak ada Tuhan (yang berhak disembah) melainkan Dia Yang Hidup kekal lagi terus menerus mengurus (makhluk-Nya); tidak mengantuk dan tidak tidur. Kepunyaan-Nya apa yang di langit dan di bumi. Tiada yang dapat memberi syafa'at di sisi Allah tanpa izin-Nya? Allah mengetahui apa-apa yang di hadapan mereka dan di belakang mereka, dan mereka tidak mengetahui apa-apa dari ilmu Allah melainkan apa yang dikehendaki-Nya. Kursi Allah meliputi langit dan bumi. Dan Allah tidak merasa berat memelihara keduanya, dan Allah Maha Tinggi lagi Maha Besar.” (QS. Al Baqoroh: 255)"),
              const Padding(
                padding: EdgeInsets.all(5),
              ),
              CircleAvatar(
                radius: 35,
                child: IconButton(
                  icon: Icon(
                    isPlaying ? Icons.pause : Icons.play_arrow,
                  ),
                  iconSize: 50,
                  onPressed: () async {
                    if (isPlaying) {
                      await audioPlayer.pause();
                    } else {
                      // await audioPlayer.resume();
                      String url =
                          'https://berdakwah.com/wp-content/uploads/2019/09/2AlBaqarah-255ayatKursi.mp3';
                      await audioPlayer.play(url);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        // floatingActionButton: FloatingActionButton(
        //     onPressed: () async {
        //       if (isPlaying) {
        //         await audioPlayer.pause();
        //       } else {
        //         // await audioPlayer.resume();
        //         String url =
        //             'https://berdakwah.com/wp-content/uploads/2019/09/2AlBaqarah-255ayatKursi.mp3';
        //         await audioPlayer.play(url);
        //       }
        //     },
        //     backgroundColor: const Color.fromARGB(255, 6, 245, 209),
        //     // tooltip: 'Next',
        //     child: Icon(isPlaying ? Icons.pause : Icons.play_arrow)),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: Theme(
          data: Theme.of(context)
              .copyWith(iconTheme: IconThemeData(color: Colors.grey.shade300)),
          child: CurvedNavigationBar(
            index: index,
            key: navigationKey,
            backgroundColor: Colors.transparent,
            color: Colors.grey.shade300,
            buttonBackgroundColor: Colors.blue.shade700,
            height: 60,
            animationCurve: Curves.easeInOut,
            animationDuration: const Duration(milliseconds: 300),
            items: items,
            onTap: (index) => setState(() => this.index = index),
          ),
        ),
      ),
    );
  }

  Widget buildWelcome(String nama) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Al-Kursi',
            style: TextStyle(
                fontFamily: 'BrandonText', fontSize: 16, color: Colors.white),
          ),
          Text(
            nama,
            style: const TextStyle(
                fontFamily: 'BrandonText',
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ],
      );
}
