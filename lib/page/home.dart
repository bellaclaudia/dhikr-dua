import 'package:flutter/material.dart';
import 'package:flutter_inamart/page/list.dart';
import 'package:flutter_inamart/navigation_drawer.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  int index = 0;
  List<Menu> menus = [
    const Menu(
        judul: 'Mornings',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Evening',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Before Sleep',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Salah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'After Salah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Ruqyah & Illness',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Praises of Allah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Salawat',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Quranic Duas',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Sunnah Duas',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Istighfar',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Dhikr for All Times',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Names of Allah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Waking Up',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Nightmares',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Clothes',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Lavatory & Wudu',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Food & Drink',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Home',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Adhan & Masjid',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Istikharah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Gatherings',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Difficulties & Happiness',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Protection of Iman',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Hajj & Umrah',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Travel',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Money & Shopping',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Social Interactions',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Marriage & Children',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Death',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
    const Menu(
        judul: 'Nature',
        urlAvatar:
            'https://image.shutterstock.com/image-vector/3d-religion-element-collection-islamic-600w-1945193149.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
    final items = <Widget>[
      const Icon(
        Icons.home,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.favorite_outline,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.note_alt,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.emoji_emotions,
        size: 30,
        color: Colors.white,
      ),
      const Icon(
        Icons.calendar_today,
        size: 30,
        color: Colors.white,
      ),
    ];
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Scaffold(
        drawer: const NavigationDrawer(),
        extendBody: true,
        appBar: AppBar(
          backgroundColor: Colors.blue.shade700,
          centerTitle: true,
          elevation: 0,
          title: const Text('Home', textAlign: TextAlign.center),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.search_outlined),
              // tooltip: 'Show Snackbar',
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: MySearchDelegate(),
                );
              },
            ),
            IconButton(
              icon: const Icon(Icons.settings_applications_sharp),
              // tooltip: 'Show Snackbar',
              onPressed: () {},
            ),
          ],
        ),
        body: ListView.builder(
          itemCount: menus.length,
          itemBuilder: (context, index) {
            final menu = menus[index];
            return Card(
              child: ListTile(
                leading: CircleAvatar(
                  radius: 16,
                  backgroundImage: NetworkImage(menu.urlAvatar),
                ),
                title: Text(menu.judul),
                trailing: const Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ListPage(menu: menu),
                    ),
                  );
                },
              ),
            );
          },
        ),
        bottomNavigationBar: Theme(
          data: Theme.of(context)
              .copyWith(iconTheme: IconThemeData(color: Colors.grey.shade300)),
          child: CurvedNavigationBar(
            index: index,
            key: navigationKey,
            backgroundColor: Colors.transparent,
            color: Colors.grey.shade300,
            buttonBackgroundColor: Colors.blue.shade700,
            height: 60,
            animationCurve: Curves.easeInOut,
            animationDuration: const Duration(milliseconds: 300),
            items: items,
            onTap: (index) => setState(() => this.index = index),
          ),
        ),
      ),
    );
  }
}

class Menu {
  final String judul;
  final String urlAvatar;

  const Menu({
    required this.judul,
    required this.urlAvatar,
  });
}

class MySearchDelegate extends SearchDelegate {
  List<String> searchResults = ['Al-Kursi', '3 Quls', 'Sayyid al-Istighfar'];

  @override
  Widget? buildLeading(BuildContext context) => IconButton(
        onPressed: () => close(context, null),
        icon: const Icon(Icons.arrow_back),
      );

  @override
  List<Widget>? buildActions(BuildContext context) => [
        IconButton(
          onPressed: () {
            if (query.isEmpty) {
              close(context, null);
            } else {
              query = '';
            }
          },
          icon: const Icon(Icons.clear),
        ),
      ];

  @override
  Widget buildResults(BuildContext context) => Center(
        child: Text(
          query,
          style: const TextStyle(fontSize: 64, fontWeight: FontWeight.normal),
        ),
      );

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> suggestions = searchResults.where((searchResult) {
      final result = searchResult.toLowerCase();
      final input = query.toLowerCase();

      return result.contains(input);
    }).toList();

    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) {
        final suggestion = suggestions[index];

        return ListTile(
          title: Text(suggestion),
          onTap: () {
            query = suggestion;

            showResults(context);
          },
        );
      },
    );
  }
}
