// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_inamart/navigation_drawer.dart';

class EmotionsPage extends StatelessWidget {
  const EmotionsPage({super.key});

  @override
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: const Text('Emotions'),
          backgroundColor: Colors.blue.shade700,
        ),
      );
}
