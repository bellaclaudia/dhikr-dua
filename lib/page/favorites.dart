import 'package:flutter/material.dart';
import 'package:flutter_inamart/navigation_drawer.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.blue.shade700,
          centerTitle: true,
          elevation: 0,
          title: const Text('Favorites', textAlign: TextAlign.center),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.settings_applications_sharp),
              // tooltip: 'Show Snackbar',
              onPressed: () {},
            ),
          ],
        ),
      );
}
